# -*- coding: utf-8 -*-

import mysql.connector
import sys


class StorageHandler:
    def __init__(self, host='localhost', port=3306, username='root', password=None, database=None, table='messages'):
        self.cnx = None
        self.cursor = None
        try:
            self.cnx = mysql.connector.connect(user=username, password=password, host=host, database=database, port=port)

        except mysql.connector.Error as err:
            print("MySQL Error ("+str(err.errno)+"): " + err.msg)
            sys.exit()
        finally:
            if self.cnx is not None:
                self.cursor = self.cnx.cursor()
                self.table=table

    def exists(self, idx, tag):
        query = 'SELECT COUNT(*) FROM `%s` WHERE `index` = %s AND `tag` = %s'
        self.cursor.execute(query, (self.table, idx, tag))
        for count in self.cursor:
            if count[0] > 0:
                return True
        return False

    def write(self, idx, message, tag):
        add_message = 'INSERT INTO `messages` (`index`, `message`, `tag`) VALUES (%s, %s, %s)'
        try:
            self.cursor.execute(add_message, (idx, message, tag))
        except mysql.connector.Error as err:
            print(err)
            return False
        else:
            self.cnx.commit()
            return True

    def get(self, idx, tag):
        message = ''
        query = 'SELECT message FROM `messages` WHERE `index` = %s AND `tag` = %s'
        delete_message = 'DELETE FROM `messages` WHERE `index` = %s AND `tag` = %s'
        if self.exists(idx, tag):
            try:
                self.cursor.execute(query, (idx,  tag))
                for result in self.cursor:
                    message = result[0]
            except mysql.connector.Error as err:
                print(err)
                return False
            else:
                try:
                    self.cursor.execute(delete_message, (idx,tag))
                except mysql.connector.Error as err:
                    print(err)
                    return False
                else:
                    self.cnx.commit()
            return message
        else:
            return False

    def __del__(self):
        if self.cnx is not None:
            self.cursor.close()
            self.cnx.close()
