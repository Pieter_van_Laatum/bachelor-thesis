# -*- coding: utf-8 -*-
import os
from pathlib import *

class StorageError(Exception):
    pass

class StorageHandler:
    def __init__(self, path='messages', prefix='', extension=''):
        self.path = path
        self.prefix = prefix
        self.extension = extension

    def filepath(self, name):
        return Path(self.path) / self.prefix / name / self.extension

    def exists(self, tag):
        return self.filepath(tag).is_file()

    def write(self, tag, message):
        path = self.filepath(tag)
        try:
            file = path.open('w+b')
            file.write(message)
            file.close()
        except OSError:
            raise StorageError('ERROR: Could not write message to filesystem')

    def get(self, tag):
        path = self.filepath(tag)
        try:
            file = path.open()
            message = file.read()
            file.close()
            os.remove(bytes(path))
        except OSError:
            raise StorageError('ERROR: Could read message from filesystem')
        else:
            return message
