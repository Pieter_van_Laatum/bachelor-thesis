#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cgi
import json
import sys
import time
import urllib.parse
import hashlib
import sha3

from FileStorage import StorageHandler, StorageError
from http.server import HTTPServer, BaseHTTPRequestHandler

HOST = ''
PORT = 8000
PROTOCOL = "HTTP/1.1"


class MessageHandler(BaseHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        self.storage = StorageHandler('/home/pieter/dev/Bachelor/messages')
        super(MessageHandler, self).__init__(*args, **kwargs)


    def do_HEAD(self):
        self.send_response(405)
        self.send_header("content-type", "text/html")
        self.send_header("allowed", "POST")
        self.end_headers()

    def do_GET(self):
        self.send_response(405)
        self.send_header("content-type", "text/html")
        self.send_header("allowed", "POST")
        self.end_headers()

    def do_POST(self):
        if self.headers['content-type'] is not None:
            ctype, pdict = cgi.parse_header(self.headers['content-type'])
            if ctype == 'multipart/form-data':
                postvars = cgi.parse_multipart(self.rfile, pdict)
            elif ctype == 'application/x-www-form-urlencoded':
                length = int(self.headers['content-length'])
                postvars = urllib.parse.parse_qs(self.rfile.read(length), keep_blank_values=1)
            else:
                postvars = {}
        else:
            postvars = {}
        if self.path == '/write':
            if b'u' in postvars and b'B' in postvars:
                message = postvars[b'u'][0]
                tag = postvars[b'B'][0].decode('utf-8').lower().strip()
                if not self.storage.exists(tag):
                    try:
                        self.storage.write(tag, message)
                    except StorageError:
                        print('ERROR - Could not write message to storage')
                        self.send_error(500)
                    else:
                        self.send_response(200)
                        self.send_header('content-type', 'application/json')
                        self.end_headers()
                        self.wfile.write(json.dumps(['success']).encode('utf-8'))
                else:
                    self.send_error(400, 'Tag already exists')
            else:
                self.send_error(400, 'Malformed data')
        elif self.path == '/get':
            if b'b' in postvars:
                tag = postvars[b'b'][0]
                s = hashlib.sha3_512()
                s.update(tag)
                tag = s.hexdigest()
                if self.storage.exists(tag):
                    try:
                        message = self.storage.get(tag)
                    except StorageError:
                        self.send_error(500)
                        print('ERROR - Could not get message from storage')
                    else:
                        self.send_response(200)
                        self.send_header("content-type", "application/json")
                        self.end_headers()
                        self.wfile.write(json.dumps([{'message': message}]).encode("utf-8"))
                else:
                    self.send_error(404)
            else:
                self.send_error(400, 'Malformed data')
        else:
            self.send_error(400)


if __name__ == "__main__":
    server = HTTPServer
    httpd = server((HOST, PORT), MessageHandler)
    httpd.protocol_version = PROTOCOL
    print(time.asctime(), "Server Starts - %s:%s" % (HOST, PORT))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print(time.asctime(), "Server Stops - %s:%s" % (HOST, PORT))
